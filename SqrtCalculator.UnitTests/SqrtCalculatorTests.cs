﻿namespace Otus.UnitTest.Sqrt.Tests;

public class Tests
{
    private SqrtCalculator calculator;
    private double epsison = 1e-7; 
    
    [SetUp]
    public void Setup()
    {
        calculator = new SqrtCalculator();
    }

    [Test]
    [Description("3. Для уравнения x^2+1 = 0 корней нет (возвращается пустой массив)")]
    public void Solve_Case3_NoRoots()
    {
        var result = calculator.Solve(1, 0, 1, epsison);
        Assert.That(result, Is.Empty);
    }

    [Test]
    [Description("5. Для уравнения x^2-1 = 0 есть два корня кратности 1 (x1=1, x2=-1)")]
    public void Solve_Case5_2Roots()
    {
        var result = calculator.Solve(1, 0, -1, epsison);
        Assert.Multiple(() =>
        {
            Assert.That(result, Has.Length.EqualTo(2));
            Assert.That(Math.Abs(result[0] - 1.0), Is.LessThan(epsison));
            Assert.That(Math.Abs(result[1] - -1.0), Is.LessThan(epsison));
        });
    }

    [Test]
    [Description("7. Для уравнения x^2+2x+1 = 0 есть один корень кратности 2 (x1= x2 = -1)")]
    public void Solve_Case7_OneRoot()
    {
        var result = calculator.Solve(1, 2, 1, epsison);
        Assert.Multiple(() =>
        {
            Assert.That(result, Has.Length.EqualTo(2));
            Assert.That(Math.Abs(result[0] - -1.0), Is.LessThan(epsison));
            Assert.That(Math.Abs(result[1] - -1.0), Is.LessThan(epsison));
        });
    }

    [Test]
    [Description("9. Коэффициент a не может быть равен 0")]
    public void Solve_Case9_AisZero_Exception()
    {
        try
        {
            calculator.Solve(0, 2, 1, epsison);
            Assert.Fail("Ожидается исключение ArgumentOutOfRangeException");
        }
        catch (ArgumentOutOfRangeException exc)
        {
            Assert.Multiple(() =>
            {
                Assert.That(exc.ParamName, Is.EqualTo("a"));
                Assert.That(exc.Message, Is.EqualTo("Параметр a должен быть отличен от 0 (Parameter 'a')"));
            });
        }
    }

    [Test]
    [Description("13. Обработка нечислового значние decimal.NaN")]
    public void Solve_Case13_NaN()
    {
        try
        {
            calculator.Solve(0, double.NaN, 1, epsison);
            Assert.Fail("Ожидается исключение ArgumentOutOfRangeException");
        }
        catch (ArgumentOutOfRangeException exc)
        {
            Assert.Multiple(() =>
            {
                Assert.That(exc.ParamName, Is.EqualTo("b"));
                Assert.That(exc.Message, Is.EqualTo("Нечисловое значение параметра (Parameter 'b')"));
            });
        }
    }
    
    [Test]
    [Description("13. Обработка нечислового значние decimal.NegativeInfinity")]
    public void Solve_Case13_NegativeInfinity()
    {
        try
        {
            calculator.Solve(0, 1, double.NegativeInfinity, epsison);
            Assert.Fail("Ожидается исключение ArgumentOutOfRangeException");
        }
        catch (ArgumentOutOfRangeException exc)
        {
            Assert.Multiple(() =>
            {
                Assert.That(exc.ParamName, Is.EqualTo("c"));
                Assert.That(exc.Message, Is.EqualTo("Нечисловое значение параметра (Parameter 'c')"));
            });
        }
    }
    
    [Test]
    [Description("13. Обработка нечислового значние decimal.PositiveInfinity")]
    public void Solve_Case13_PositiveInfinity()
    {
        try
        {
            calculator.Solve(double.PositiveInfinity, 1, 1, epsison);
            Assert.Fail("Ожидается исключение ArgumentOutOfRangeException");
        }
        catch (ArgumentOutOfRangeException exc)
        {
            Assert.Multiple(() =>
            {
                Assert.That(exc.ParamName, Is.EqualTo("a"));
                Assert.That(exc.Message, Is.EqualTo("Нечисловое значение параметра (Parameter 'a')"));
            });
        }
    }
    
}