﻿namespace Otus.UnitTest.Sqrt;

public class SqrtCalculator
{
    private void NotANumberCheck(string paramName, double value)
    {
        if (double.IsNaN(value) || double.IsInfinity(value))
        {
            throw new ArgumentOutOfRangeException(paramName, "Нечисловое значение параметра");
        }
    }
    
    public double[] Solve(double a, double b, double c, double epsilon)
    {
        NotANumberCheck(nameof(a), a);
        NotANumberCheck(nameof(b), b);
        NotANumberCheck(nameof(c), c);
        
        if (Math.Abs(a) < epsilon)
        {
            throw new ArgumentOutOfRangeException(nameof(a), "Параметр a должен быть отличен от 0");
        }
        
        var d = b * b - 4 * a * c;
        // нет действительных корней
        if (d < -epsilon)
        {
            return Array.Empty<double>();
        }
        
        // есть 2 действительных корня кратности 1
        if (d > epsilon)
        {
            return new[]
            {
                (-b + Math.Sqrt(d)) / (2 * a),
                (-b - Math.Sqrt(d)) / (2 * a)
            };
        }
        
        // есть один корень кратности 2
        return new[] { -b / (2 * a), -b / (2 * a) };
    }
}